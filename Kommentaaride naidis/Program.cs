﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// see on rea kommentaar, roheline (ignoreeritav) on kõik kuni rea lõpuni

/* See on ploki kommentaar
 kommentaar on kõik kuni bloki lõpuni*/

namespace EsimeneKonsool
{
    class Program // klassi sees saab olla muutujaid, ei saa olla lauseid. Klasside nimed alati SUURE TÄHEGA.
    {
        static void Main(string[] args) // siit läheb programm käima - peale seda sisu, enne seda ära kirjuta sisu
        {
            Console.WriteLine("Tere Mari!");  

            //* 
            Console.WriteLine("Täna on ilus ilm");
            //*/ Console.WriteLine("Täna on kole ilm");

            //esimene ja põhiline mõiste on MUUTUJA
            //muutuja on nimeline mälupiirkond

            int arv = 7; // muutuja defineerimine ja väärtustamine
            int teinearv; // muutuja defineerimine ilma väärtustamata (halb komme)
            teinearv = arv * 10; // muutuja väärtustamine (avaldisega)
            Console.WriteLine(teinearv); //muutuja kasutamine (väljanäitamiseks)

            // teine fundamentaalmõiste - ANDMETÜÜP
            int // andmetüüp
            kolmasArv // muuruja nimi
            = 100 // algväärtus (avaldis)
            ; // lõpetab lause

            // ANDMETÜÜP - defineerib nelja asja
            // 1. kui palju bitte vaja on - MAHT
            // 2. mida need bitid tähendavad - SEMANTIKA
            // 3. väärtuste hulk - SÜNTAKS, DOMAIN
            // 4. mida nendega teha saab- operatsioonid, tehted jms.

            // C# ANDMETÜÜP ON KLASS JA KLASS ON ANDMETÜÜP
            // Java On andmetüübid, mis on klassid, ja mis pole klassid - primitiivid
            // Primitiivid on väga lihtsad andmetüübid (nt. arv)

            Console.WriteLine(kolmasArv.GetType().FullName);

            decimal d1 = 10;
            Decimal d2 = d1;

            DateTime täna = DateTime.Today;
            Console.WriteLine(täna);

            char a = 'A'; // charil on 65 000 väärtust
            Console.WriteLine(++a);
            Console.WriteLine('a' - 'A');

            //AVALDIS - lausend, mis on pandud kokku:
            //literaalidest - arvud ja muud konstandid
            //muutujatest
            //tehetest
            //sulgudest
            //funktsioonidest
            // pole omistamislauset, on omistamis avaldis
            //liitmistehtel on tulemusel andmetüüp

            Console.WriteLine((arv = teinearv)* 7);

            Console.WriteLine(10 / 7); // kui mõlemal pool on täis arv, siis vastus samuti täisarv
            Console.WriteLine(10% 7); //tulemus on jagamise jääk


            /* TEHTED 
            Binaarsed tehted - kaks osapoolt, keskel tehtemärk, 2 operandi
	        1. Lihttehtemärgid - + / % *   
            2. Liittehtemärgid +=  -=  *= 
            a) mida see tehe teeb - tulemus
            b) mida see tehe veel teeb - kõrvalefekt

            Unaarsed tehted - üks operand (avaldis miinus arv, loetakse 0 miinus arv, vastandarvu tehe)
            ! - vastupidise vastuse tehe
            ++ võta järgmine
            -- võta eelmine

            Ternaarne tehe - kolm operandi
            ?: 
            Elvise tehe:
            boolean avaldis ? jah-väärtus : ei väärtus

            =  omistamistehe 
            == võrdlemistehe
            === samasustehe (seda C# pole)
            */

            //ELVISE NÄIDE
            Console.Write("Palju palka saad: ");
            decimal palk = decimal.Parse(Console.ReadLine());
            string tulemus = palk < 1000 ? "vaene" : "rikas";
            string kuhu =
                palk > 1000 ? "õhtust sööma" :
                palk > 100 ? "kinno" :
                palk > 10 ? "burksi" :
                "mitte kuhugi";
            Console.WriteLine("sinuga lähme " + kuhu);

            kolmasArv += (arv += 7) *2; //muutuja arv läheb seitsme võrra suuremaks
            Console.WriteLine((arv = kolmasArv) + (teinearv * arv));

            arv = 20;
            Console.WriteLine(arv++); //20 - kasutatakse arvu eelmist väärtust
            Console.WriteLine(++arv); //22 - kasutatakse ühe võrra suurendatud väärtust



        }
    }
}
 